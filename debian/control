Source: golang-github-mwitkow-go-http-dialer
Section: golang
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Sophie Brun <sophie@offensive-security.com>
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
Standards-Version: 4.6.2
Vcs-Browser: https://gitlab.com/kalilinux/packages/golang-github-mwitkow-go-http-dialer
Vcs-Git: https://gitlab.com/kalilinux/packages/golang-github-mwitkow-go-http-dialer.git
Homepage: https://github.com/mwitkow/go-http-dialer
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/mwitkow/go-http-dialer
Testsuite: autopkgtest-pkg-go

Package: golang-github-mwitkow-go-http-dialer-dev
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
Description: Go net.Dialer for HTTP(S) CONNECT Tunneling. (library)
 A net.Dialer drop-in that establishes the TCP connection over an HTTP
 CONNECT Tunnel
 (https://en.wikipedia.org/wiki/HTTP_tunnel#HTTP_CONNECT_tunneling).
 .
 Some enterprises have fairly restrictive networking environments. They
 typically operate HTTP forward proxies
 (https://en.wikipedia.org/wiki/Proxy_server) that require user
 authentication. These proxies usually allow  HTTPS (TCP to :443) to pass
 through the proxy using the CONNECT
 (https://tools.ietf.org/html/rfc2616#section-9.9) method. The CONNECT
 method is basically a HTTP-negotiated "end-to-end" TCP stream... which is
 exactly what net.Conn (https://golang.org/pkg/net/#Conn) is :)
